xdffileio (1.0-1) UNRELEASED; urgency=medium

  [ Mohammed Bilal ]
  * Add autopkgtest
    Closes: #1009158
  * d/t/control : Update Test depends

  [ Andreas Tille ]
  * New upstream version from Github
  * Adapt watch file
  * Remove trailing whitespace in debian/rules (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove field Section on binary package libxdffileio0 that duplicates source.
  * Build-Depends: pkg-config
  * Add override for libmmlib1-dev
  * Provide "Section: libs" for the library package (needed by d-shlibs) and
    make sure lintian-brush will not remove it as a duplicate from source
    package by setting the source to "Section: science"
  * Build-Depends: pkg-config, python3-dev, python3-numpy
  * Build-Depends: check
  * Standards-Version: 4.6.2 (routine-update)

  [ Nilesh Patra ]
  * The mmlib APIs seem to be wrong at many places in the package. Fix the
    same with a patch.
  * Add B-D on libmmlib-dev
  * Add two bin packages (xdffileio-bin and python3-pyxdf) and add in
    install files for them
  * Cleanup __pycache__ in d/rules
  * Update symbols file with new symbols
  * Prune all .la files from being installed
  * Remove debian revision on symbol file

 -- Nilesh Patra <nilesh@debian.org>  Fri, 13 Oct 2023 20:50:18 +0000

xdffileio (0.3-4) unstable; urgency=medium

  * Team upload.
  * Add d/libxdffileio-dev.maintscript to move /usr/share/doc/libxdffileio-dev
    from symbolic link toward libxdffileio0, to a separate directory.
    Closes: #985290

 -- Étienne Mollier <etienne.mollier@mailoo.org>  Wed, 17 Mar 2021 13:51:56 +0100

xdffileio (0.3-3) unstable; urgency=medium

  [ Andreas Tille ]
  * Team upload.
  * Take over package into Debian Med team maintenance
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Secure URI in copyright format (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Use secure URI in Homepage field.
  * Fix day-of-week for changelog entry 0.1-0.
  * Build on hurd
    Closes: #761898
  * d-shlibs - but why is static lib not built?

  [ Nilesh Patra ]
  * Enable static lib to be built
  * Properly rename manpages file
  * Clean leftover files after build with overriding dh_auto_clean
  * Do not conflict and install examples twice
  * Remove license copy from install

 -- Andreas Tille <tille@debian.org>  Sat, 19 Dec 2020 19:14:04 +0100

xdffileio (0.3-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix frequent parallel FTBFS by using the serial test harness.
    (Closes: #912056)

 -- Adrian Bunk <bunk@debian.org>  Mon, 10 Dec 2018 10:41:39 +0200

xdffileio (0.3-2) unstable; urgency=medium

  * Team upload.
  * Fake watch file since download area vanished upstream
  * Drop manual -dbg file since this is automatically generated
  * Maintained in Neurodebian team
  * Priority: optional
  * debhelper 10
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Tue, 18 Sep 2018 21:53:47 +0200

xdffileio (0.3-1) unstable; urgency=low

  * New upstream: xdffileio-0.3
  * Drop all patches: have been integrated in upstream.
  * Bump Debian policy version to 3.9.3 -- no changes.
  * Use versioned copyright format
  * Bump debhelper compatibility level to 9.
  * Add multiarch support.

 -- Nicolas Bourdaud <nicolas.bourdaud@gmail.com>  Tue, 08 May 2012 15:39:55 +0200

xdffileio (0.2-3) unstable; urgency=low

  * Fix kfreebsd build:
    new patch workaround-freebsd-bug-in-errorcheck.patch
  * Fix Hurd build:
    new patch skip-errorcheck-if-setrlimit-broken.patch

 -- Nicolas Bourdaud <nicolas.bourdaud@gmail.com>  Mon, 06 Feb 2012 02:20:36 +0100

xdffileio (0.2-2) unstable; urgency=low

  * Applies ignore-SIGXFSZ-signal-in-errorcheck-text.patch: this fix the
    build on kfreebsd architectures.
  * Upload sponsored by Michael Hanke <mih@debian.org>

 -- Nicolas Bourdaud <nicolas.bourdaud@gmail.com>  Wed, 18 Jan 2012 00:15:18 +0100

xdffileio (0.2-1) unstable; urgency=low

  * Update maintainer email
  * Switch to 3.0 (quilt) format
  * DEP5 compliant debian/copyright.
  * Make all docs installed in the doc folder of libxdffileio0
  * Bump Standards-version to 3.9.2
  * Tighten build-dependency on debhelper to >= 7.0.50~, due to _override
    rules.
  * Provides a -dbg package
  * Update optional fields in control (Vcs-Git, Vcs-Browser, Homepage)
  * Add debian/watch
  * Only provide an unversioned libxdffileio-dev package.
  * Specify compiler and linker flags
  * Run dh with autotools-dev
  * Improves package description.
  * Build depends on dpkg-dev (>= 1.16.1~), due to the inclusion of
    /usr/share/buildflags.mk in debian/rules.
  * Applies fix-glitches-in-manpages.patch: fix hyphen used as minus and
    spelling mistakes.
  * Adds "-ffloat-store" to CFLAGS for i386 and m68k cpu architectures

  * Upload sponsored by Michael Hanke <mih@debian.org>
  * Initial release. (Closes: #646954)

 -- Nicolas Bourdaud <nicolas.bourdaud@gmail.com>  Wed, 21 Dec 2011 12:43:23 +0100

xdffileio (0.2-0) lucid; urgency=low

  * Stop using cdbs.
  * Reduce locking overhead
  * Relicense to LGPL3

 -- Nicolas Bourdaud <nicolas.bourdaud@epfl.ch>  Sun, 18 Sep 2011 10:16:51 +0200

xdffileio (0.1-0) lucid; urgency=low

  * Remove dependency_libs from libtool files (*.la).

 -- Nicolas Bourdaud <nicolas.bourdaud@epfl.ch>  Wed, 18 May 2011 16:27:00 +0200

xdffileio (0.1) lucid; urgency=low

  * Initial Release.

 -- Nicolas Bourdaud <nicolas.bourdaud@epfl.ch>  Fri, 29 Apr 2011 18:36:26 +0200
